
# Lua Localizer

Lua localizer is a program written in C# which has been made to localize Lua 5.1 code.

## Quick brief example:
```lua
local zero, nine, comma = string.byte('09,', 1, 3)
-- Parses a comma separated list of numbers into numbers
local function parser(input)
    return function()
        local len = #input
        local numstart = nil

        for idx = 1, len do
            -- Read the char as a byte since it's more efficient
            -- than a plain tostring
            local ch = string.byte(input, idx)

            -- If we have a decimal char, then do nothing other
            -- than set the start position if it's not set
            if ch >= zero and ch <= nine then
                if numstart == nil then
                    numstart = idx
                end
            elseif ch == comma then
                -- Otherwise, if we have a start set, return the parsed
                -- number and set the starting position back to nil
                if numstart ~= nil then
                    return tostring(string.sub(input, numstart, idx))
                    numstart = nil
                end
            else
                error(string.format("Invalid character '%c' found in input.", string.char(ch)))
            end
        end
    end
end
```

Will be modified into this:
```lua
local string_byte,string_sub,string_format,string_char,tostring,error = string.byte,string.sub,string.format,string.char,tostring,error
local zero, nine, comma = string_byte('09,', 1, 3)
-- Parses a comma separated list of numbers into numbers
local function parser(input)
    return function()
        local len = #input
        local numstart = nil

        for idx = 1, len do
            -- Read the char as a byte since it's more efficient
            -- than a plain tostring
            local ch = string_byte(input, idx)

            -- If we have a decimal char, then do nothing other
            -- than set the start position if it's not set
            if ch >= zero and ch <= nine then
                if numstart == nil then
                    numstart = idx
                end
            elseif ch == comma then
                -- Otherwise, if we have a start set, return the parsed
                -- number and set the starting position back to nil
                if numstart ~= nil then
                    return tostring(string_sub(input, numstart, idx))
                    numstart = nil
                end
            else
                error(string_format("Invalid character '%c' found in input.", string_char(ch)))
            end
        end
    end
end
```
## Known issues
- Not converting custom functions E.g.: "function test() end" to "local function test() end" 
- ^ THIS IS BEING WORKED ON RIGHT NOW!
## Usage

To use Lua Localizer you will simply have to go to the lua_localizer.exe file which is being located inside lua_localizer/bin/Debug/net6.0/

Next you edit the input.txt file and paste whatever LUA script you want inside.

Now you drag and drop the input.txt file into the lua_localizer.exe file and wait for about 2 seconds.

To retrieve the localized script open the output.lua and copy everything.
## Authors

- [@vin.rocks](https://www.gitlab.com/vin.rocks)
