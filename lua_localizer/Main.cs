﻿using lua_localizer.Functions;

namespace lua_localizer.CLI
{
    class MainProgram
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("LL ERROR: No file input detected. Please drag the .txt/.lua file into the lua_localizer.exe file in order to localize your script.");
                Console.ReadLine();
                return;
            }
            var filePath = args[0];
            if (!File.Exists(filePath))
            {
                Console.WriteLine("LL ERROR: File does not exist. Check the specified file path.");
                Console.ReadLine();
                return;
            }

            if (File.ReadAllText(filePath) == "")
            {
                Console.WriteLine("LL ERROR: File seems to be empty. Check the value of the file specified!");
                Console.ReadLine();
                return;
            }
            Localizer.Localize(filePath);
            Console.ReadLine();
        }
    }
}