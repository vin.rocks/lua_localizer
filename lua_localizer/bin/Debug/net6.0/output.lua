local string_byte,string_sub,string_format,string_char,tostring,error = string.byte,string.sub,string.format,string.char,tostring,error
local zero, nine, comma = string_byte('09,', 1, 3)
-- Parses a comma separated list of numbers into numbers
local function parser(input)
    return function()
        local len = #input
        local numstart = nil

        for idx = 1, len do
            -- Read the char as a byte since it's more efficient
            -- than a plain tostring
            local ch = string_byte(input, idx)

            -- If we have a decimal char, then do nothing other
            -- than set the start position if it's not set
            if ch >= zero and ch <= nine then
                if numstart == nil then
                    numstart = idx
                end
            elseif ch == comma then
                -- Otherwise, if we have a start set, return the parsed
                -- number and set the starting position back to nil
                if numstart ~= nil then
                    return tostring(string_sub(input, numstart, idx))
                    numstart = nil
                end
            else
                error(string_format("Invalid character '%c' found in input.", string_char(ch)))
            end
        end
    end
end
