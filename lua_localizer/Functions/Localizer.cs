﻿namespace lua_localizer.Functions
{
    internal class Localizer
    {
        public static void Localize(string input)
        {
            string[] tableFunctions = TableFunctionCollector.Collect(input);
            string[] customFunctions = CustomFunctionCollector.Collect(input);
            string[] functions = FunctionCollector.Collect(input);
            string variableAssembled = VarAssemble.Assemble(tableFunctions, functions);
            File.WriteAllText("output.lua", ReplaceFunctions.Replace(input, customFunctions, variableAssembled));
            Console.WriteLine("Successfully localized script. The output is in output.lua!");
        }
    }
}
