﻿using System.Text.RegularExpressions;

namespace lua_localizer.Functions
{
    internal class FunctionCollector
    {
        public static string[] Collect(string input)
        {
            List<string> functionsCollected = new List<string>();
            string[] functions = { "xpcall", "package", "tostring", "print", "unpack", "require", "getfenv", "setmetatable", "next", "assert", "tonumber", "rawequal", "collectgarbage", "getmetatable", "module", "rawset", "pcall", "newproxy", "type", "coroutine", "_G", "select", "gcinfo", "pairs", "rawget", "loadstring", "ipairs", "_VERSION", "dofile", "setfenv", "load", "error", "loadfile" };
            var lines = File.ReadAllLines(input);

            for (var i = 0; i < lines.Length; i += 1)
            {
                var line = lines[i];
                for (var k = 0; k < functions.Length; k += 1)
                {
                    var currentFunc = functions[k];
                    string funcDetectionPattern = currentFunc + @"\(";
                    Regex funcRegex = new Regex(funcDetectionPattern);
                    var detectedFunc = funcRegex.Match(line).ToString();
                    if (detectedFunc != "")
                    {
                        if (!functionsCollected.Contains(currentFunc))
                        {
                            functionsCollected.Add(currentFunc);
                        }
                    }
                }
            }
            return functionsCollected.ToArray();
        }
    }
}

