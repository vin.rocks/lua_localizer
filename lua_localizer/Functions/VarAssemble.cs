﻿namespace lua_localizer.Functions
{
    internal class VarAssemble
    {
        public static string Assemble(string[] tblFuncs, string[] funcs)
        {
            string varName = "";
            string varValue = "";

            for (int i = 0; i < tblFuncs.Length; i+=1)
            {
                string convertedFuncName = ConvertFunctionName.Convert(tblFuncs[i]);
                varName = varName + convertedFuncName + ",";
                varValue = varValue + tblFuncs[i] + ",";
            }
            for (int i = 0; i < funcs.Length; i += 1)
            {
                string convertedFuncName = ConvertFunctionName.Convert(funcs[i]);
                varName = varName + convertedFuncName + ",";
                varValue = varValue + funcs[i] + ",";
            }

            varName = varName.Remove(varName.Length - 1);
            varValue = varValue.Remove(varValue.Length - 1);

            string assembledVars = "local " + varName + " = " + varValue;

            return assembledVars;
        }
    }
}
