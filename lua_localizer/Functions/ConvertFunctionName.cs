﻿namespace lua_localizer.Functions
{
    internal class ConvertFunctionName
    {
        public static string Convert(string input)
        {
            if (input == "") return "NULL";
            string tempString = input;
            if (input.Contains("."))
            {
                tempString = tempString.Replace(".", "_");
            }

            return tempString;
        }
    }
}
