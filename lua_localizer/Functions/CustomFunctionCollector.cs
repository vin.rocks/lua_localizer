﻿using System.Text.RegularExpressions;

namespace lua_localizer.Functions
{
    public class CustomFunctionCollector
    {
        public static string[] Collect(string input)
        {
            List<string> functionsCollected = new List<string>();

            var lines = File.ReadAllLines(input);
            for (var i = 0; i < lines.Length; i += 1)
            {
                var line = lines[i];
                string customFuncDetectionPattern = @"function \w+\(";
                
                Regex funcRegex = new Regex(customFuncDetectionPattern);

                var detectedFunction = funcRegex.Match(line).ToString();

                if(!line.Contains("local function"))
                {
                    if (detectedFunction != "")
                    {
                        if (!functionsCollected.Contains(detectedFunction))
                        {
                            functionsCollected.Add(detectedFunction);
                        }
                    }
                }

            }
            return functionsCollected.ToArray();
        }
    }
}
