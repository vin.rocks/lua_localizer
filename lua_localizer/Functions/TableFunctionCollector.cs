﻿using System.Text.RegularExpressions;

namespace lua_localizer.Functions
{
    internal class TableFunctionCollector
    {
        public static string[] Collect(string input)
        {
            List<string> functionsCollected = new List<string>();

            var lines = File.ReadAllLines(input);
            for (var i = 0; i < lines.Length; i += 1)
            {
                var line = lines[i];
                string funcTableDetectionPattern = @"\w+\.\w+\([\s+\S+]+|\[]\)";
                string funcNamePattern = @"\w+\.\w+";

                Regex funcRegex = new Regex(funcTableDetectionPattern);
                Regex funcNameRegex = new Regex(funcNamePattern);

                var detectedLine = funcRegex.Match(line).ToString();
              
                var tempFunc = detectedLine;
                do
                {
                    var detectedFuncName = funcNameRegex.Match(tempFunc).ToString();
                    if (detectedFuncName != "")
                    {
                        if (!functionsCollected.Contains(detectedFuncName))
                        {
                            functionsCollected.Add(detectedFuncName);
                        }
                        tempFunc = tempFunc.Replace(detectedFuncName, " ");
                    }
                    
                } while(funcRegex.IsMatch(tempFunc));
            }
            return functionsCollected.ToArray();
        }
    }
}
