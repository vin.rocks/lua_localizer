﻿using System.Text.RegularExpressions;

namespace lua_localizer.Functions
{
    internal class ReplaceFunctions
    {
        public static string Replace(string input, string[] customFunctions, string varsAssembled)
        {
            var lines = File.ReadAllLines(input);
            List<string> newLinesList = new List<string>();
         
            for (int i = 0; i < lines.Length; i+=1)
            {
                var line = lines[i];
                string funcTableDetectionPattern = @"\w+\.\w+\([\s+\S+]+|\[]\)";
                string funcNamePattern = @"\w+\.\w+";
                Regex funcNameRegex = new Regex(funcNamePattern); 
                Regex funcRegex = new Regex(funcTableDetectionPattern);
                do
                {
                    var detectedFuncName = funcNameRegex.Match(line).ToString();
                    if (detectedFuncName != "")
                    {
                        line = line.Replace(detectedFuncName, ConvertFunctionName.Convert(detectedFuncName));
                    }
                } while (funcRegex.IsMatch(line));
                newLinesList.Add(line);
            }
            string[] newLines = newLinesList.ToArray();
            var script = "";
            for (int i = 0; i < newLines.Length; i+=1)
            {
                script = script + newLines[i] + "\n";
            }

            script = varsAssembled + "\n" + script;
            return script;
        }
    }
}
